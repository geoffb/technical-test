# Script to transform an access log file to Json
# Package Log2Json
# Geoffroy Baumier - 10/2019
# https://gitlab.com/geoffb/technical-test
import re

def readFile():
    with open("../data/epa-http.txt", "r") as f:
        return f.readlines()

def processFile(data):
    number_lines = 0
    number_corrupted = 0
    corrupted = []
    content = ""

    for line in data:
        number_lines += 1
        line = re.sub('[\x00-\x1F]+', '', line)
        line_pieces = line.split()

        content += line_pieces[0] + " "

        content += line_pieces[1] + " "

        # Missing protocol and method
        if len(line_pieces) is 5 :
            content += "\"NA "
            content += line_pieces[2].replace('"', '') + " "
            content += "NA/NA\" "

        # Missing protocol
        elif len(line_pieces) is 6 :
            content += "\"" + line_pieces[2].replace('"', '') + " "
            content += line_pieces[3].replace('"', '') + " "
            content += "NA/NA\" "

        # Perfect lenght, no problem
        elif len(line_pieces) is 7 :
            content += "\"" + line_pieces[2].replace('"', '') + " "
            content += line_pieces[3].replace('"', '') + " "

            protocol_pieces = line_pieces[4].replace('"', '').split("/")
            content += protocol_pieces[0] + "/"
            content += protocol_pieces[1] + "\" "
        
        # Space in url
        elif len(line_pieces) > 7 :
            url = ""
            protocol = ""
            protocol_version = ""
            last_bit_request = len(line_pieces) - 3

            for k in range (3, last_bit_request + 1):
                if k is last_bit_request:
                    if "HTTP" in line_pieces[k]:
                        protocol_pieces = line_pieces[k].replace('"', '').split("/")
                        protocol = protocol_pieces[0]
                        protocol_version = protocol_pieces[1]
                        url = url[:-3]
                    else:
                        url += line_pieces[k].replace('"', '')
                        protocol = "NA"
                        protocol_version = "NA"
                else:
                    url += line_pieces[k] + "%20"

            content += "\"" + line_pieces[2].replace('"', '') + " "
            content += url + " "
            content += protocol + "/"
            content += protocol_version + "\" "

        else:
            number_corrupted += 1
            corrupted.append(line)

        content += line_pieces[len(line_pieces) - 2] + " "          
        content += line_pieces[len(line_pieces) - 1] + " \n"

    print "Number of lines in document: ", number_lines
    print "Number of lines corrupted: ", number_corrupted
    print("Lines corrupted:")
    print(corrupted)

    return content

def generateFile(content):
    print("\nCreating clean file...")

    file = open("../data/epa-http-clean.txt", "w")
    file.write(content)
    file.close()

    print("\nDone!")

def createJsonFile():
    print "\n\nStarting cleaning file...\n"
    data = readFile()
    content = processFile(data)
    generateFile(content)

if __name__ == '__main__':
    createJsonFile()