# Script that reads the log file and count the corrupted lines
# Package Log2Json
# Geoffroy Baumier - 10/2019
# https://gitlab.com/geoffb/technical-test

def init():
    number_lines = 0
    number_corrupted = 0
    number_under_six = 0

    with open("/Users/geoffroybaumier/Code/Technical test/data/epa-http.txt", "r") as f:
        data = f.readlines()

    for line in data:
        number_lines += 1
        line_pieces = line.split()

        if len(line_pieces) not in [6, 7]:
            number_corrupted += 1
            print("\n-------------------\n")
            print "Line ", number_lines
            print "Length", len(line_pieces), "\n\n"
            
            line_pieces[2] = line_pieces[2].replace('"', '')
            
            for item in line_pieces:
                print "=> ", item
        
        if len(line_pieces) is 6:
            number_under_six += 1

    print "\n\nNumber of lines in document: ", number_lines
    print "Number of lines corrupted: ", number_corrupted
    print "Number of lines with only 6 elements: ", number_under_six
