# Script to transform an access log file to Json
# Package Log2Json
# Geoffroy Baumier - 10/2019
# https://gitlab.com/geoffb/technical-test
import re

def readFile():
    with open("/data/epa-http.txt", "r") as f:
        return f.readlines()

def processFile(data):
    number_lines = 0
    number_corrupted = 0
    corrupted = []
    content = "[\n"

    for line in data:
        number_lines += 1
        line = re.sub('[\x00-\x1F]+', '', line)
        line_pieces = line.split()

        content += "\t{\n"

        content += "\t\t\"host\": \""
        content += line_pieces[0]
        content += "\",\n"

        datetime = line_pieces[1].replace('[', '').replace(']', '')
        datetime_pieces = datetime.split(":")
        content += "\t\t\"datetime\": {\n"
        content += "\t\t\t\"day\": \"" + datetime_pieces[0] + "\",\n"
        content += "\t\t\t\"hour\": \"" + datetime_pieces[1] + "\",\n"
        content += "\t\t\t\"minute\": \"" + datetime_pieces[2] + "\",\n"
        content += "\t\t\t\"second\": \"" + datetime_pieces[3] + "\"\n"
        content += "\t\t},\n"

        content += "\t\t\"request\": {\n"

        # Missing protocol
        if len(line_pieces) is 5 :
            content += "\t\t\t\"method\": \"NA\",\n"
            content += "\t\t\t\"url\": \"" + line_pieces[2].replace('"', '') + "\",\n"
            content += "\t\t\t\"protocol\": \"NA\",\n"
            content += "\t\t\t\"protocol_version\": \"NA\"\n"

        # Missing protocol and method
        elif len(line_pieces) is 6 :
            content += "\t\t\t\"method\": \"" + line_pieces[2].replace('"', '') + "\",\n"
            content += "\t\t\t\"url\": \"" + line_pieces[3].replace('"', '') + "\",\n"
            content += "\t\t\t\"protocol\": \"NA\",\n"
            content += "\t\t\t\"protocol_version\": \"NA\"\n"

        # Perfect lenght, no problem
        elif len(line_pieces) is 7 :
            content += "\t\t\t\"method\": \"" + line_pieces[2].replace('"', '') + "\",\n"
            content += "\t\t\t\"url\": \"" + line_pieces[3].replace('"', '') + "\",\n"

            protocol_pieces = line_pieces[4].replace('"', '').split("/")
            content += "\t\t\t\"protocol\": \"" + protocol_pieces[0] + "\",\n"
            content += "\t\t\t\"protocol_version\": \"" + protocol_pieces[1] + "\"\n"
        
        # Space in url
        elif len(line_pieces) > 7 :
            url = ""
            protocol = ""
            protocol_version = ""
            last_bit_request = len(line_pieces) - 3

            for k in range (3, last_bit_request + 1):
                if k is last_bit_request:
                    if "HTTP" in line_pieces[k]:
                        protocol_pieces = line_pieces[k].replace('"', '').split("/")
                        protocol = protocol_pieces[0]
                        protocol_version = protocol_pieces[1]
                        url = url[:-3]
                    else:
                        url += line_pieces[k].replace('"', '')
                        protocol = "NA"
                        protocol_version = "NA"
                else:
                    url += line_pieces[k] + "%20"

            content += "\t\t\t\"method\": \"" + line_pieces[2].replace('"', '') + "\",\n"
            content += "\t\t\t\"url\": \"" + url + "\",\n"
            content += "\t\t\t\"protocol\": \"" + protocol + "\",\n"
            content += "\t\t\t\"protocol_version\": \"" + protocol_version + "\"\n"

        else:
            number_corrupted += 1
            corrupted.append(line)

        content += "\t\t},\n"

        content += "\t\t\"response_code\": \""
        content += line_pieces[len(line_pieces) - 2]                

        content += "\",\n"

        content += "\t\t\"document_size\": \""
        content += line_pieces[len(line_pieces) - 1]
        content += "\"\n"

        content += "\t},\n"

    print "Number of lines in document: ", number_lines
    print "Number of lines corrupted: ", number_corrupted
    print("Lines corrupted:")
    print(corrupted)

    return content[:-2] + "\n]"

def generateFile(content):
    print("\n\nGenerating Json file...")

    file = open("/data/result.json", "w")
    file.write(content)
    file.close()

    print("\nDone!")

def createJsonFile():
    data = readFile()
    content = processFile(data)
    generateFile(content)