# Main script of the project, start the process
# Geoffroy Baumier - 10/2019
# https://gitlab.com/geoffb/technical-test

from log2json_package import log2json, read_file_count

print("\nWelcome to the Log2Json program\n")
print("------------------------------------------\n\n")

#read_file_count.init()
log2json.createJsonFile()