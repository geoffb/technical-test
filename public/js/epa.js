var numberTotal = 0;
var dataPointsMethods = {};
var dataPointsResponses = {};
var dataPointsRequestsPerMinute = {};
var dataPointsSize = {};
var dataPieMethods = [];
var dataResponses = [];
var dataRequests = [];
var dataSize = [];

$(document).ready(() => {
	$.getJSON("data/result.json", addData);
});

function addData(data) {
	$('#loader').hide();

	numberTotal = data.length;

	for (var i = 0; i < data.length; i++) {
		// Sum of the methods
		addToDadaPoints(dataPointsMethods, data[i].request.method);

		// Sum of response code
		addToDadaPoints(dataPointsResponses, data[i].response_code);

		// Requests per minute
		var minute = data[i].datetime.day + ":" + data[i].datetime.hour + ":" + data[i].datetime.minute;
		addToDadaPoints(dataPointsRequestsPerMinute, minute);

		// Size of requests
		if (data[i].response_code === "200" && parseInt(data[i].document_size) < 1000) {
			addToDadaPoints(dataPointsSize, data[i].document_size);
		}
	}
	
	// Create the methods pie percentages :
	var keysMethods = Object.keys(dataPointsMethods);

	for (var i = 0;i < keysMethods.length; i++) {
		dataPieMethods.push({
            y: (dataPointsMethods[keysMethods[i]] * 100) / numberTotal,
            label: keysMethods[i]
        });
	}

	// Create the response code pie percentages :
	var keysResponses = Object.keys(dataPointsResponses);

	for (var i = 0;i < keysResponses.length; i++) {
		dataResponses.push({
            y: (dataPointsResponses[keysResponses[i]] * 100) / numberTotal,
            label: keysResponses[i]
        });
	}

	// Create the data for request per minute :
	var keysRequests = Object.keys(dataPointsRequestsPerMinute);

	for (var i = 0;i < keysRequests.length; i++) {
		explode_date = keysRequests[i].split(":");

		dataRequests.push({
			x: new Date(1995, 8, explode_date[0], explode_date[1], explode_date[2]),
			y: dataPointsRequestsPerMinute[keysRequests[i]]
        });
	}

	// Create distribution of size data :
	var keysSize = Object.keys(dataPointsSize);

	for (var i = 0;i < keysSize.length; i++) {
		dataSize.push({
            x: keysSize[i],
            y: dataPointsSize[keysSize[i]]
        });
	}
	
	pieMethods.render();
	pieAnswersCode.render();
	chartRequests.render();
	chartSize.render();
}

function addToDadaPoints(dataPointsObject, data) {
	// Test if the key exists in Object
	var key_not_exists = dataPointsObject[data] === undefined;

	if (key_not_exists) {
		// Create the key
		dataPointsObject[data] = 1;
	}
	else {
		// Count + 1
		dataPointsObject[data] += 1;
	}
}

// Chart for the distribution of HTTP methods
var pieMethods = new CanvasJS.Chart("pieMethods", {
	animationEnabled: true,
	title: {
		text: "Distribution of HTTP methods"
	},
	data: [{
		type: "pie",
		startAngle: 240,
		yValueFormatString: "##0.00\"%\"",
		indexLabel: "{label} {y}",
		dataPoints: dataPieMethods
	}]
});

// Chart for the distribution of response code
var pieAnswersCode = new CanvasJS.Chart("pieAnswersCode", {
	animationEnabled: true,
	title: {
		text: "Distribution of HTTP answer codes"
	},
	data: [{
		type: "pie",
		startAngle: 240,
		yValueFormatString: "##0.00\"%\"",
		indexLabel: "{label} {y}",
		dataPoints: dataResponses
	}]
});

// Chart requests
var chartRequests = new CanvasJS.Chart("chartRequests", {
	theme: "light1",
	animationEnabled: true,
	zoomEnabled: true,
	title: {
		text: "Requests per minute | Try Zooming and Panning"
	},
	axisY: {
		title: "Number of requests"
	},
	axisX: {
		title: "Hours"
	},
	data: [{
		type: "area",
		dataPoints: dataRequests,
		yValueFormatString: "#0.## Requests",
		xValueFormatString: "DD - HH:MM",
		toolTipContent: "x: {x}, y: {y}"
	}]
});

// Chart size of requests
var chartSize = new CanvasJS.Chart("chartSize", {
	animationEnabled: true,
	theme: "light1",
	title:{
		text: "Distribution of the size of the answer of all requests with code 200 and size < 1000B"
	},
	axisY:{
		includeZero: false,
		title: "Number of requests"
	},
	axisX:{
		minimum: 0,
		maximum: 1000,
		title: "Size of requests"
	},
	data: [{        
		type: "line",       
		dataPoints: dataSize
	}]
});