import { getData, dataPieMethods, dataResponses, dataRequests, dataSize } from './data'

const init = () => {
    // Get and prepare data
    getData();

    // Draw the charts
    drawMethodesPie();
    drawResponsesPie();
    drawRequestPerMinuteChart();
    drawSizeDistributionChart();
}

// Chart for the distribution of HTTP methods
const drawMethodesPie = () => {
    var pieMethods = new CanvasJS.Chart("pieMethods", {
        animationEnabled: true,
        title: {
            text: "Distribution of HTTP methods"
        },
        data: [{
            type: "pie",
            startAngle: 240,
            yValueFormatString: "##0.00\"%\"",
            indexLabel: "{label} {y}",
            dataPoints: dataPieMethods
        }]
    });

    pieMethods.render();
}

// Chart for the distribution of response code
const drawResponsesPie = () => {
    var pieAnswersCode = new CanvasJS.Chart("pieAnswersCode", {
        animationEnabled: true,
        title: {
            text: "Distribution of HTTP answer codes"
        },
        data: [{
            type: "pie",
            startAngle: 240,
            yValueFormatString: "##0.00\"%\"",
            indexLabel: "{label} {y}",
            dataPoints: dataResponses
        }]
    });

    pieAnswersCode.render();
}

// Chart requests per minute
const drawRequestPerMinuteChart = () => {
    var chartRequests = new CanvasJS.Chart("chartRequests", {
        theme: "light1",
        animationEnabled: true,
        zoomEnabled: true,
        title: {
            text: "Requests per minute | Try Zooming and Panning"
        },
        axisY: {
            title: "Number of requests"
        },
        axisX: {
            title: "Hours"
        },
        data: [{
            type: "area",
            dataPoints: dataRequests,
            yValueFormatString: "#0.## Requests",
            xValueFormatString: "DD - HH:MM",
            toolTipContent: "x: {x}, y: {y}"
        }]
    });

    chartRequests.render();
}

// Chart size of requests
const drawSizeDistributionChart = () => {
    var chartSize = new CanvasJS.Chart("chartSize", {
        animationEnabled: true,
        theme: "light1",
        title:{
            text: "Distribution of the size of the answer of all requests with code 200 and size < 1000B"
        },
        axisY:{
            includeZero: false,
            title: "Number of requests"
        },
        axisX:{
            minimum: 0,
            maximum: 1000,
            title: "Size of requests"
        },
        data: [{        
            type: "line",       
            dataPoints: dataSize
        }]
    });

    chartSize.render();
}

export { init }