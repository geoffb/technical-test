import { addToDadaPoints } from './dataHelper';

const json = require('../json/result.json');

let numberTotal = 0;

let dataPointsMethods = {};
let dataPointsResponses = {};
let dataPointsRequestsPerMinute = {};
let dataPointsSize = {};

var dataPieMethods = [];
var dataResponses = [];
var dataRequests = [];
var dataSize = [];

const getData = () => {
    numberTotal = json.length;

    for (let jsonObject of json) {
        // Distribution of methods
        dataPointsMethods = addToDadaPoints(dataPointsMethods, jsonObject.request.method);

        // Distribution of response code
        dataPointsResponses = addToDadaPoints(dataPointsResponses, jsonObject.response_code);

        // Requests per minute
		let minute = jsonObject.datetime.day + ":" + jsonObject.datetime.hour + ":" + jsonObject.datetime.minute;
        dataPointsRequestsPerMinute = addToDadaPoints(dataPointsRequestsPerMinute, minute);
        
        // Size of requests
		if (jsonObject.response_code === "200" && parseInt(jsonObject.document_size) < 1000) {
			dataPointsSize = addToDadaPoints(dataPointsSize, jsonObject.document_size);
		}
    }

    createMethodsData();
    createResponsesData();
    createRequestMinute();
    createSizeDistribution();
}

// Create the methods pie percentages :
const createMethodsData = () => {
    for (let methodPoint in dataPointsMethods) {
        dataPieMethods.push({
            y: (dataPointsMethods[methodPoint] * 100) / numberTotal,
            label: methodPoint
        });
    }
}

// Create the response code pie percentages :
const createResponsesData = () => {
    for (let responsePoint in dataPointsResponses) {
        dataResponses.push({
            y: (dataPointsResponses[responsePoint] * 100) / numberTotal,
            label: responsePoint
        });
    }
}

// Create the data for request per minute :
const createRequestMinute = () => {
	for (let requestMinutePoint in dataPointsRequestsPerMinute) {
		let explode_date = requestMinutePoint.split(":");

		dataRequests.push({
			x: new Date(1995, 8, explode_date[0], explode_date[1], explode_date[2]),
			y: dataPointsRequestsPerMinute[requestMinutePoint]
        });
	}
}

// Create distribution of size data :
const createSizeDistribution = () => {
	for (let sizeDistPoint in dataPointsSize) {
		dataSize.push({
            x: sizeDistPoint,
            y: dataPointsSize[sizeDistPoint]
        });
	}
}

export { getData, dataPieMethods, dataResponses, dataRequests, dataSize }