const addToDadaPoints = (dataPointsObject, data) => {
	// Test if the key exists in Object
	let key_not_exists = dataPointsObject[data] === undefined;

	if (key_not_exists) {
		// Create the key
		dataPointsObject[data] = 1;
	}
	else {
		// Count + 1
		dataPointsObject[data] += 1;
    }
    
    return dataPointsObject;
}

export { addToDadaPoints }