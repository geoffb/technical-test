/**
 * Start the process, call init from epa.js
 */
import { init } from './epa';

require('./css');

init();