<?php

/**
 * Command CleanLogCommand
 * Run with: bin/console app:clean-log
 * Start the process of cleaning and parsing a log file
 */

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use \App\Service\ReaderLog;

class CleanLogCommand extends Command
{
    protected $reader;
    
    public function __construct(ReaderLog $reader) {
        parent::__construct();
        $this->reader = $reader;
        
    }
    protected function configure() {
        $this->setName('app:clean-log');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $output->writeln('Welcome to the Log parsing program!');
        
        // Pass the OutputInterface to the reader
        $this->reader->run($output);
        
        $output->writeln('End of program.');
    }
}