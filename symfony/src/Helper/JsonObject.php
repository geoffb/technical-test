<?php

/**
 * Helper Class JsonObject
 * With the different methods, setters and getters and it contains also
 * two stdClass for the datetime and the request.
 */

namespace App\Helper;

class JsonObject {
    public $host;
    
    public $datetime;
    
    public $request;
    
    public $response_code;
    
    public $document_size;

    public function setHost(string $host): void {
        $this->host = $host;
    }

    public function getHost(): string {
        return $this->host;
    }

    public function setDatetime(string $datetime): void {
        $datetimeParts = explode(':', str_replace(array('[', ']'), '', $datetime));
        $this->datetime = new \stdClass();

        $this->datetime->day = $datetimeParts[0];
        $this->datetime->hour = $datetimeParts[1];
        $this->datetime->minute = $datetimeParts[2];
        $this->datetime->second = $datetimeParts[3];
    }

    public function getDatetime(): object {
        return $this->datetime;
    }

    public function setRequest(string $requestPart1, string $requestPart2, string $requestPart3): void {
        $this->request = new \stdClass();

        $this->request->method = str_replace('"', '', $requestPart1);
        $this->request->url = htmlspecialchars(preg_replace('/[\x00-\x1F\x7F]/', '', $requestPart2));

        $protocolParts = explode('/', str_replace('"', '', $requestPart3));
        $this->request->protocol = $protocolParts[0];
        $this->request->protocol_version = $protocolParts[1];
    }

    public function getRequest(): object {
        return $this->request;
    }

    public function setResponseCode(string $responseCode): void {
        $this->response_code = $responseCode;
    }

    public function getResponseCode(): string {
        return $this->response_code;
    }

    public function setDocumentSize(string $documentSize): void {
        $this->document_size = str_replace("\n", "", $documentSize);
    }

    public function getDocumentSize(): string {
        return $this->document_size;
    }
}