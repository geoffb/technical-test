<?php

/**
 * Service WriterJson
 * Writes the data received to a Json file
 */

namespace App\Service;

use Symfony\Component\Config\Definition\Exception\Exception;

class WriterJson {
    protected $outputFilename = 'output-symfony.json';
    protected $lines = array();
    protected $path;
    
    public function __construct(string $path) {
        $this->path = $path;
    }

    public function addLine($line) {
        $this->lines[] = $line;
    }

    public function getOutputFile() {
        return $this->path . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . $this->outputFilename;
    }

    public function openWriteFile() {
        if (!file_exists($this->getOutputFile())) {
            throw new Exception('File not found');
        }

        return fopen($this->getOutputFile(), 'w+');
    }

    public function write() {
        $lines = '';
        $handle = $this->openWriteFile();

        // Headers UTF8
        fwrite($handle, chr(0xEF) . chr(0xBB) . chr(0xBF));
        fwrite($handle, '[');

        foreach($this->lines as $line) {
            $lines .= json_encode($line).',';
        }

        fwrite($handle, rtrim($lines, ','));
        fwrite($handle, ']');
        fclose($handle);
    }
}