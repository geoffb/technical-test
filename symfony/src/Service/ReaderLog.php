<?php

/**
 * Service ReaderLog
 * Reads the log file and sends it to the writer service
 */

namespace App\Service;

use App\Helper\JsonObject;
use App\Service\WriterJson;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class ReaderLog {

    protected $inputName = 'data_epa-http.txt';
    protected $path;
    protected $writer;
    protected $output;
    protected $totalLines = 0;
    protected $invalidLines = 0;

    public function __construct(string $path, WriterJson $writer) {
        $this->path = $path;
        $this->writer = $writer;
    }

    protected function getInputFile() {
        return $this->path . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . $this->inputName;
    }

    public function openReadFile() {
        if (!file_exists($this->getInputFile())) {
            throw new Exception('File not found');
        }

        return fopen($this->getInputFile(), "r");
    }

    /**
     * Main/Logging function
     */
    public function run(OutputInterface $output) {
        $output->writeln('> Opening log file');
        $handle = $this->openReadFile();

        while (($line = fgets($handle)) !== false) {
            $result = $this->parseLine($line);

            if (!$result) {
                $output->writeln('<comment>Invalid line (' . $this->totalLines . '): ' . $line . '</comment>');
            }

            $this->totalLines++;
        }

        $output->writeln('Number of lines in log file: ' . $this->totalLines);
        $output->writeln('Number of invalid lines: ' . $this->invalidLines);

        $output->writeln('> Writing Json file...');
        $this->writer->write();
        
        $output->writeln('<info>Done!</info>');
    }

    /**
     * Parsing function: parse line from json and return bool
     */
    protected function parseLine($line) {
        $parts = explode(' ', $line);

        if (count($parts) === 7) {
            $jsonObject = new JsonObject();

            $jsonObject->setHost($parts[0]);
            $jsonObject->setDatetime($parts[1]);
            $jsonObject->setRequest($parts[2], $parts[3], $parts[4]);
            $jsonObject->setResponseCode($parts[5]);
            $jsonObject->setDocumentSize($parts[6]);

            $this->writer->addLine($jsonObject);
            return true;
        }
        else {
            $this->invalidLines++;
            // Service error queue

            return false;
        }
    }
}