# Technical test UFirst

This repository contains the results of UFirst's technical test: Server access analysis. The assignment had two tasks: writing a script that takes the data from a log file to put in a Json file, and create a web page displaying the data. To present the results I chose GitLab which I use in every project. I wrote this page with markdown.

1. [Write a script](#write-a-script) 
    * [Python script](#write-a-script)
        * [How to use](#how-to-use)
    * [Logstash](#logstash)
        * [Clean the file](#clean-the-file)
        * [Convert to Json](#convert-to-json)
        * [How to use LogstashJson](#how-to-use-logstashjson)
2. [Create a webpage](#create-a-webpage)
    * [Using HTML and Javascript](#using-html-and-javascript)
        * [Using the template directory](#using-the-template-directory)
        * [Creating a Javascript file](#creating-a-javascript-file)
        * [Creating the Graphics with a library](#creating-the-graphics-with-a-library)
    * [ELK Stack](#elk-stack)
        * [Why I chose this solution](#why-i-chose-this-solution)
        * [Creating the stack](#creating-the-stack)
        * [Feeding ES with the data](#feeding-es-with-the-data)
        * [Configuring Kibana](#configuring-kibana)
        * [Creating the Graphics](#creating-the-graphics)
        * [Going further](#going-further)
3. [Annexes](#annexes)
4. [Auto critic](#auto-critic)
5. [Credits](#credits)
6. [Update](#update)
    * [Backend](#backend)
        * [Why this approach](#why-this-approach)
        * [PHP using Symfony](#php-using-symfony)
    * [Frontend](#frontend)
        * [My approach](#my-approach)
        * [ES6 using Webpack](#es6-using-webpack)
    * [Closing thoughts](#closing-thoughts)

## Write a script

The goal is to write a script that takes the data from an access log file, cleans it, verify it and write a Json file. There are two different ways to approach this problem. The first one would be the "quick and dirty" way to have a fast result. And there is the more suitable way which is using tools ready to use and that are open-source. For the first part I chose to use Python as it is a perfect language to manipulate data. For the second part I chose Logstash because I heard you are using Elastic Search, so why not use the stack!

* [Python script](#write-a-script)
    * [How to use](#how-to-use)
* [Logstash](#logstash)
    * [Clean the file](#clean-the-file)
    * [Convert to Json](#convert-to-json)
    * [How to use LogstashJson](#how-to-use-logstashjson)

### Python script

We can see that the log has this format:

    141.243.1.172 [29:23:53:25] "GET /Software.html HTTP/1.0" 200 1497

To generalize:
    
    <Request host> [<Day>:<Hour>:<Minute>:<Second>] "<Method> <Url> <Protocol>/<Protocol version>" <Response code> <Document size>

We can count 11 element separated in 7 columns if we divide by space. So very quickly I made a script to see if my hypothesis was correct. Keep in mind that this is pure exploration of the file with a handwritten script.

![](images/read_file_count.png)

With a [first script](python_scripts/log2json_package/read_file_count.py) I separated the ones with the correct number of columns (7) and the lines with only 6 columns (missing the protocol/protocol_version column). The other lines that didn't have 6 or 7 columns were printed to have a bird eye view of the problem(s). 
We can count 47748 lines in the document, with most of the lines having the correct number of columns and 129 corrupted lines: 81 are missing a procol/protocol_version and 48 other lines have a different number of columns, all above 7. I'm immediately guessing that there might be some spaces in the urls and update my script to parse the space to '%20' the url encoded space character. 

The [second python script](python_scripts/log2json_package/log2json.py) does what the first task asks, which is to create a json file from the log file. From the 129 corrupted lines, we correct the lines with 6 columns and we get the 48 with more than 7 columns. I saw that they all had a response code and a size, I was sure of that. So I test the 3rd element from the end to see if it was the procol/protocol_version. If yes, I had all I needed and if not, all the rest was a url and i was missing a procol/protocol_version column as illustrated in the script below.

![](images/test_http.png)

To finish I create a package called log2json wich is lunched from a main python script. And to make it even easier I create an executable .sh file. To start the script to create the Json file we just need to type: 

    $ ./log2json.sh 

And we get the result:

![](images/log2json.png)

A [Json file is created](data/result.json) in the data directory. But now let's test it. I decided to use an online tool called [JsonLint](https://jsonlint.com). I had my doubt and clearly enough the file didn't pass the test because of some characters found. 

![](images/test_ko.png)

It turns out they were control characters, I updated [my script](python_scripts/log2json_package/log2json.py) and used a regex to trim them out of the file.

![](images/control_chars.png)

And surely enough the test was passed and we have a [Json file created](data/result.json) with all the data cleared of any unwanted characters.

![](images/test_ok.png)

Here is an example object of the result, formatted as asked.

![](images/result_json.png)

This is a fun and exploratory way of creating from scratch and in a few lines of code (~120 lines) a script that can do the job. This is why I like Python. But in a professional environment this is not really possible and we must think of better ways to do it.

#### How to use:

To test this script easily and without installing python, I packaged everything in a [Docker image](https://hub.docker.com/r/geoffroybaumier/pythonimage). To do so I built a Docker image from a simple [Dockerfile](docker_files/python/Dockerfile), copied my files to it and published it to Docker hub.

**Go to a terminal and type the following command to download the image.**

    $ docker pull geoffroybaumier/pythonimage:1.0

**Then the python script will directly be launched at the creation of the container.**

    $ docker run --name python geoffroybaumier/pythonimage:1.0

    Welcome to the Log2Json program

    ------------------------------------------


    Number of lines in document:  47748
    Number of lines corrupted:  0
    Lines corrupted:
    []


    Generating Json file...

    Done!

**Or if you want to go and inspect the files you can create a container and attached it to a bash command.**

    $ docker create -it --name python geoffroybaumier/pythonimage:1.0 bash
    96837d49a74a54c04f216e496e58f80cad71108c2e512df454d68b17bda8952f
    $ docker start python
    python
    $ docker exec -it python bash
    root@96837d49a74a:/# ./python_scripts/log2json.sh 

    Welcome to the Log2Json program

    ------------------------------------------


    Number of lines in document:  47748
    Number of lines corrupted:  0
    Lines corrupted:
    []


    Generating Json file...

    Done!

**The file will be located at the root of the container.**

    $ ls -la /data/result.json
    -rw-r--r-- 1 root root 15431912 Oct  5 14:14 /data/result.json

### Logstash

During my interview with Andreas Meier, he told me about the company using Elastic search. And I thought why not use the stack to solve the problem. So I got interested in the ELK Stack and here particularly into Logstash, an open source tool to collect, analyse and stock logs.

#### Clean the file

Before processing the file with Logstash, we need to clean the file. But with the work done before we exactly know what is wrong with the file. I can just use the same method as seen above to clean my file. I decided to create another file as output of the cleaning function to keep the given file intact.

To do so, I created another python file named [clean_file.py](python_scripts/clean_file.py). We can directly execute the script with a command line as follow:

    $ python clean_file.py

We get this output below. The output data will be stored in a text file: [epa-http-clean.txt](data/epa-http-clean.txt).

![](images/clean_file.png)

#### Convert to Json

Now we can use Logstash to create a Json file. Logstash works like a middleware, it is listening for an input and will give an output after processing the data. Logstash will need a configuration file for this to work. I created this [configuration file](logstash_confs/logstash_json.conf) to transform the cleaned data into a Json file. The file is using the grok parsing syntax. 

    input {
        file {
            path => "/Users/geoffroybaumier/Code/Technical test/data/epa-http-clean.txt"
            start_position => "beginning"
        }
    }

    filter {
        grok {   
            match => { 
                "message" => "%{URIHOST:request_host} \[%{DATA:date}:%{HOUR:hour}:%{MINUTE:minute}:%{SECOND:second}\] \"%{DATA:method} %{DATA:url} %{URIPROTO:protocol}\/%{DATA:protocol_version}\" %{BASE10NUM:response_code} %{DATA:document_size} "
            }
        }
    }

    output {
        stdout { 
            codec => rubydebug
        }
        file {
            path => "/Users/geoffroybaumier/Code/Technical test/data/logstash_output.json"
        }
    }

I can start Logstash process using my configuration file with this command:

    $ ~/logstash-7.4.0/bin/logstash -f logstash_confs/logstash_json.conf 

As you can see, Logstash created the [Json file](data/logstash_output.json), and I also added a output in the console. The structure of the Json object is not quite the same as I never found how to structure the output, but all the data is there and named properly. This will help us later during the different graphics creation.

![](images/logstash_json_results.png)

#### How to use LogstashJson

You can try the conversion using a Docker image I created with this [Dockerfile](docker_files/logstash_json/Dockerfile). You can find the Docker image [here](https://hub.docker.com/r/geoffroybaumier/logstash_json).

**Go to a terminal and type the following command to download the image.**

    $ docker pull geoffroybaumier/logstash_json:1.0

**Then create a container from this image.**

    $ docker create -it --name logstash_json geoffroybaumier/logstash_json:1.0 bash

**Once created you can go in and try Logstash with the following commands.**

    $ docker start logstash_json
    $ docker exec -it logstash_json bash

    bash-4.2$ bin/logstash -f /logstash_confs/logstash_json_docker.conf

**You can find the Json document at the root of the container.**

To stop Logstash to listen we first need to send a signal to stop the process, use ctrl+c.

    bash-4.2$ ls -la /data/
    total 182420
    drwxr-xr-x 1 root root      4096 Oct  6 15:43 .
    drwxr-xr-x 1 root root      4096 Oct  6 16:04 ..
    -rw-r--r-- 1 root root   4497618 Oct  5 19:18 epa-http-clean.txt
    -rw-rw-rw- 1 root root 182285137 Oct  6 16:07 logstash_output.json

## Create a webpage

The goal here is to use the file created and display its data to analyse it and show 4 graphics, using HTML and Javascript. Here too, I chose to answer with two solutions, one is developed from scratch using Javascript and HTML/CSS. The other way is to use very powerful tools to analyse the data. 

* [Using HTML and Javascript](#using-html-and-javascript)
    * [Using the template directory](#using-the-template-directory)
    * [Creating a Javascript file](#creating-a-javascript-file)
    * [Creating the Graphics with a library](#creating-the-graphics-with-a-library)
* [ELK Stack](#elk-stack)
    * [Why I chose this solution](#why-i-chose-this-solution)
    * [Creating the stack](#creating-the-stack)
    * [Feeding ES with the data](#feeding-es-with-the-data)
    * [Configuring Kibana](#configuring-kibana)
    * [Creating the Graphics](#creating-the-graphics)
    * [Going further](#going-further)

### Using HTML and Javascript

I will now develop a [webpage](https://geoffb.gitlab.io/technical-test) that will show 4 graphics :
- Requests per minute over the entire time span
- Distribution of HTTP methods (GET, POST, HEAD, ...)
- Distribution of HTTP answer codes (200, 404, 302, ...)
- Distribution of the size of the answer of all requests with code 200 and size < 1000B

#### Using the template directory

I used the template directory for the development. I rename the folder public to be able to use it with the Gitlab pipelines. This will allow me to publish the website directly online whenever I commit some changes. The website can be found at this address: [https://geoffb.gitlab.io/technical-test](https://geoffb.gitlab.io/technical-test). To be able to publish it, I added a file named [.gitlab-ci.yml](.gitlab-ci.yml) at the root of my repository.

The template directory had a basic architecture. with a file HTML which I renamed [index.html](public/index.html) to be the landing page of the website. It also has a [CSS file](public/css/main.css) and a [Javascript file](public/js/epa.js).

#### Creating a Javascript file

The [Javascript file](public/js/epa.js) is using the JQuery library and an AJAX request to the Json file.
The process is very simple, I read the data file with an AJAX request. I create four data sets and render the four charts using each data set.

#### Creating the Graphics with a library

I used to work with the [D3.js](https://d3js.org/) library for different types of graphics but for this example here I chose to go with [Canvas.js](https://canvasjs.com/). 

**Requests per minute over the entire time span**

For this graphic, I chose to use an classic bar chart. This graphic is interactive, you can select a time frame and zoom on it. Here is the result:

![](images/chart_requests_per_minute.png)
![](images/chart_requests_per_minute2.png)

**Distribution of HTTP methods (GET, POST, HEAD, ...)**

For this graphic, I chose to use a pie chart. The main reason is the majority of the methods where GET (96.37%), a pie chart is more representative than a bar chart. The pie is clickable and each slice has a legend. Here is the result:

![](images/pie_methods.png)

**Distribution of HTTP answer codes (200, 404, 302, ...)**

For this graphic, I also chose to use a pie chart. The pie is also clickable.

![](images/pie_answer_codes.png)

**Distribution of the size of the answer of all requests with code 200 and size < 1000B*

For the last graphic, I chose to use a bar chart. This one only has an interactive pointer. Here is the result:

![](images/chart_200.png)

### ELK Stack

Here we will see how to answer the same question with a more appropriated solution, I will create an ELK Stack using docker and then data will be sent from Logstash to the Elastic search database. Then Kibana will be used to access the data and make the graphics.

#### Why I chose this solution

Why is this solution more appropriated than the custom one? Here we are using tools that are developed to do the exact thing we want. The solution is also open-source, scalable and there is documentation.
I also chose this solution because I wanted to try using ES and the ELK Stack for a long time and when I heard you were using it, I got excited and I wanted to use it!

#### Creating the stack

I will create the stack using Docker. The first step is to create the Elastic search database. We can easily do so using the already made Elastic search Docker image. 

**Elastic search**

Let's start by downloading the image.

    $ docker pull docker.elastic.co/elasticsearch/elasticsearch:7.4.0

Then we need to create a container, using the following command. I'm running the container in detached mode, I opened the ports needed and I named it elasticsearch (where dd156dd42341 is Elastic search image ID).

    $ docker run -d -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" --name elasticsearch dd156dd42341

The container should be up now, we can quickly test if elasticsearch is running with a curl command:

    $ curl http://localhost:9200

And we get this output:

![](images/elascticsearch_docker_up.png)

**Kibana**

Then we need to set up Kibana to manipulate and view the data from the elasticsearch container. We start by downloading the Docker image using this command:

    $ docker pull docker.elastic.co/kibana/kibana:7.4.0

Then we need to create the Kibana container. I will name it kibana, expose the default port 5601 and link the container elasticsearch (where 0328df36f79f is Kibana image ID). We can do so using this command:

    $ docker run -d -p 5601:5601 --name kibana --link elasticsearch:elasticsearch 0328df36f79f

Now we can check if the container is running with a docker ps command and we get this output:

![](images/docker_ps_ek.png)

We can open a browser and type this url [http://localhost:5601](http://localhost:5601).

![](images/kibana_empty.png)

**Logstash**

Now we need to set up our Logstash cointainer. There are a few steps in doing so, the first is to create a new logstash configuration file. Then we need to create a Dockerfile to create an image that fits us. Later we will execute Logstash and if everything goes well we should have the data coming in our Elastic search database.

Let's start with the [new Logstash configuration file](logstash_confs/logstash_import_es.conf).
It's almost the same configuration as last time except here instead of writing into our Json file, I will send the data directly to Elastic search. In my configuration file, my output will look like this:

    output {
        stdout { 
            codec => rubydebug
        }
        elasticsearch {
            hosts => ["elasticsearch:9200"]
        }
    }

Now, I will create a [Dockerfile](docker_files/logstash_es/Dockerfile) to create a Logstash image and I will include the configuration file and the data file. You can find the Docker image [here](https://hub.docker.com/r/geoffroybaumier/logstash_elasticsearch). 

    FROM docker.elastic.co/logstash/logstash:7.4.0

    COPY ["/logstash_confs/logstash_import_es.conf", "/logstash_confs/logstash_import_es.conf"]
    COPY ["/data/epa-http-clean.txt", "/data/epa-http-clean.txt"]

Now all I need to do is to build and create a container linked to the Elastic search database. I did so using this commands:

    $ docker pull geoffroybaumier/logstash_elasticsearch
    $ docker create -it --name logstash --link elasticsearch:elasticsearch geoffroybaumier/logstash_elasticsearch:1.0 bash

#### Feeding ES with the data

Now that everything is set up for our data, we need to use Logstash to import [the clean data file](data/epa-http-clean.txt) created earlier in part 1. We need to start our Docker container and launch Logstash with our custom configuration.

    $ docker start logstash
    $ docker exec -it logstash bash

Once inside, just type:

    bash-4.2$ bin/logstash -f /logstash_confs/logstash_import_es.conf

And everything will start importing into our Elastic search database. To verify that, we can go to our Kibana website located at [http://localhost:5601](http://localhost:5601) and see that it found some data!

![](images/kibana_full.png)

#### Configuring Kibana

Now we need to create a few things to be able to manipulate the data. The first thing is to create an index pattern. I used "logstash-2019.10.06*" as my pattern and chose the "timestamp" as the time filter.

![](images/kibana_step1.png)
![](images/kibana_step2.png)

Once this is done, we can go to our exploration tab. There we can see that we have 47748 items in our database, everything was imported!

![](images/kibana_count.png)

We will now create a dashboard to put our visualisations. Go to the dashboard tab, and click new dashboard. Now all we have to do is create graphics to fill this dashboard.

#### Creating the Graphics

Now let's go to the visualisations tab and start creating.
I will create a pie using my data set for the distribution of HTTP methods.

![](images/kibana_pie.png)

And here we have it:

![](images/kibana_method.png)

I will do another pie for the distribution of HTTP answer codes:

![](images/kibana_response.png)

Unfortunatly these are the only two graphics that I could do with the time given. I am confident that with more knowledge of Kibana I would be able to achieve the same goal as above with Javascript.

#### Going further

I wish I had a bit more time to learn more about Kibana and how to make more and better graphics. But in a short period of time I found how to understand and use this solution to answer the questions. But to go further, we can imagine pluging directly the logs of your different clients' website for example, and you could be able to manage, analyse and take direct conclusions from this tools, if not already used! I can't wait to see how you solve this problem in your company.

Also, I thought about creating a docker-compose.yml file but I preferred not to lose time on this and concentrate on other tasks that were more crutial.

## Annexes

Here are some very good resources I found while doing this project.

- All docker images for ELK Stack [docker.elastic.co](https://www.docker.elastic.co/)
- Configuring Logstash for Docker [elastic.co](https://www.elastic.co/guide/en/logstash/current/docker-config.html)
- Logstash Configuration Examples [elastic.co](https://www.elastic.co/guide/en/logstash/current/config-examples.html)
- Grok patterns [github.com](https://github.com/logstash-plugins/logstash-patterns-core/blob/master/patterns/grok-patterns)
- JavaScript Charts from JSON Data API and AJAX [canvasjs.com](https://canvasjs.com/docs/charts/how-to/javascript-charts-from-json-data-api-and-ajax/)

## Auto critic

- I only used the master branch on the git repo
- I worked with an outdated version of Python
- "Hack" used for document size in Logstash (" ")
- Didn't refactor the Python code. Duplicate functions

## Credits

As you can see, I put all my effort and did the necessary research to answer the test the best I could. I wish you good luck to find suitable people and I hope that I am one of them. The Valencia project is a great opportunity, is really interesting and have true potential. It was a real pleasure to do this test and I hope you enjoy reading it as mush as I did doing it.

### UFirst

I want to thank UFirst for giving me the opportunity to be part of the recruitment process.
 
I want to also thank Andreas Meier for his time and a great chat.

### Acknowledgements

The logs were collected by Laura Bottomley (​laurab@ee.duke.edu)​ of Duke University.

## Update

After having sent the results to UFirst, I got some very constructive critics from Alan Fehr. The main critic was about the backend script, my approach was not the way that you are developing software at UFirst. You gave me a second chance to create a maintainable piece of software. The second critic was about my frontend not using ES6 and the latest tools. To be franc, I was put on the wrong path by the template folder, I wish I had followed my instinct and not use the starter directory. To show you that I can use modern tools, I thought it would be fun to also re-do the frontend. 

* [Backend](#backend)
    * [Why this approach](#why-this-approach)
    * [PHP using Symfony](#php-using-symfony)
* [Frontend](#frontend)
    * [My approach](#my-approach)
    * [ES6 and Webpack](#es6-and-webpack)
* [Closing thoughts](#closing-thoughts)

### Backend

As I understand the critic, and I agree with it, the Python script is not the approach that is likely to be used by your company. You are looking for a scalable and maintainable piece of software. To process the data there are a lot of different ways, Python is one of the best language to do the job. But this time I chose to use PHP as it is one of my strengths and one of the prominent languages at UFirst.

#### Why this approach

To create this backend I used the framework Symfony 4 and the console component. This approach makes it perfect for a maintainable solution and it gives us the robustness of Symfony. This piece of software is very scalable for many reasons. The command starts a log reader packaged as a service and then another service writerJson will write the result into a Json file. If the log file changes format, we can easily update the [Json PHP Class](symfony/src/Helper/JsonObject.php) to match the new format. Or if one day we want to create another output file, Csv for example, we can create new interfaces and plug them into the reader service. Also, in the reader service, there is a function that will sort the lines with the right format from the wrong ones. Here we are working with a small file of 47748 lines but what if we are working with a file that is considerably larger? I thought of using separate queues for error handling, this has a lot of advantages. Besides preserving a "happy flow" from an error flow, it is usually better for the performance as the separate queues increase parallelism and reduce bottleneck. It is also better to separate the flows because it is easier for management, for example if we want to know how many errors there are. Let's see how it works.

#### PHP using Symfony

I added the [project Symfony](symfony) at the root of the git repository. In the structure of my source (symfony/src) folder you can find the two different services, the [JsonObject helper](symfony/src/Helper/JsonObject.php) and my [CleanLogCommand](symfony/src/Command/CleanLogCommand.php). 

![](images/symfony_structure.png)

The most important part is inside the [ReaderLog](symfony/src/Service/ReaderLog.php) service. The main run function reads the file, sends the current line to the parseLine function and depending on the result from parseLine logs the warning lines and finally starts the write service. This function is used to do the logging.

![](images/symfony_run.png)

The parseLine function creates a jsonObject if the line has a correct number of columns. Otherwise it return false, we can also imagine sending the invalid line to another service for error management (I left a comment "Service error queue"). This is the part that is the most scalable. We can imagine that instead of using this function to create a JsonObject maybe here we can send the data to another service, like Logstash for example.  

![](images/symfony_parse.png)

**How to use**

For you to be able to test the command, I packaged the project into a docker image available at this address: [https://hub.docker.com/r/geoffroybaumier/symfony](https://hub.docker.com/r/geoffroybaumier/symfony).

Let's pull the image and create a container:

    $ docker pull geoffroybaumier/symfony:1.0
    $ docker create -it --name symfony geoffroybaumier/symfony:1.0 bash

Now let's start the container named symfony and try the clean log command:

    $ docker start symfony
    $ docker exec -it symfony bash
    root@eca4c724fa5a:/# cd symfony/
    root@eca4c724fa5a:/symfony# bin/console app:clean-log
    Welcome to the Log parsing program!
    > Opening log file
    Invalid line (276): cragateway.cra.com.au [30:00:26:38] "GET /cgi-bin/waisgate?port=210&ip_address=earth1&database_name=/usr1/comwais/indexes/HTDOCS&headline=Query Report for this Search&type=TEXT&docid=/usr1/comwais/indexes/HTDOCS0 0 /tmp/08300129.fk8 HTTP/1.0" 200 169

    [...]

    Invalid line (46435): ad03-045.compuserve.com [30:22:50:38] "GET /icons/ok2-0.gif" 200 231

    Number of lines in log file: 47748
    Number of invalid lines: 129
    > Writing Json file...
    Done!
    End of program.

The program worked perfectly. We have our Json file generated in our docker container (/symfony/config/data/output-symfony.json) and we have the logs of the invalid lines in the console.

### Frontend

Well, you didn't ask me to re-do the frontend but I thought the critic was justified and I decided to create a version of the last frontend but this time using Webpack. You can find [the project](es6) at the root of this repository.

#### My approach

I chose Webpack because it is the most popular bundler right now and I like how it manages the dependencies. The structure is pretty simple, I have my [package.json](es6/package.json) and my [webpack.config.js](es6/webpack.config.js) and then in my [src](es6/src) directory I have all my files. And of course the [dist](es6/dist) directory contains the result of my build command.

![](images/es6.png)

#### ES6 and Webpack

The entry point of my code is in [src/js/index.js](es6/src/js/index.js) which calls the init function of my [epa.js](es6/src/js/epa.js). The init function start the load of the data and the parsing for the four different charts. Here is the main function from data.js:

![](images/es6_getData.png)

And then we can create the data for the charts, here for example the data for the methods distribution:

![](images/es6_dataChart.png)

Then I built everything using Webpack, I also installed Babel for retro compatibility of browsers. You can find a live version of this frontend at this address: [https://geoffb.gitlab.io/es6/](https://geoffb.gitlab.io/es6/). 

### Closing thoughts

With this update on this technical test, I hope that I answered your concerns with ideas you will find interesting. I agree that a simple python script as the first module of a data stream isn't a durable solution, it was very exploratory. This new backend using Symfony to me answers this problem in a way that it works for 47748 lines and it will work for a lot more. And I really like the scalable side of this entry point for the data, the JsonObject class makes it maintainable and makes it very readable. And, regarding the frontend, I hope that you will appreciate the remodelling of this webpage using the latest technology and tools. 
I want to thank you again for giving me a second chance to answer this assignment. I appreciate it very much and I can't wait to hear your thoughts on it.